# Oney test

## Configuration requise

| Prérequis | Vérification | Installation |
| --------- | ------------ | ------------ |
| Apache = 2.4
| MySQL = 5.7.17 | mysql --version
| Composer = 1.6.4 | composer -v | getcomposer.org
| PHP >= 7.2.x | php -v | php.net



## Usage
Remplacer '/var/www/html/Oney_test' par la répértoire de votre projet
partout dans le dossier .docker

Depuis le dossier du projet :  
1. run docker-compose build
2. run docker-compose up



## Install
1. Update your .env
2. Update API_KEY Goggle map geoloce its valable one time in the day (or we must have a payement API KEY)
3. run `composer install`
4. run `composer update`


pour accéder au formulaire :
aller sur le port 8020 de votre serveur.



en local:
localhost:8020

