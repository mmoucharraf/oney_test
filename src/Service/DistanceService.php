<?php

namespace App\Service;





class DistanceService
{

    private $apiKeyIpify;
    private $apiKeyGoogle;
    /**
     * DistanceService constructor.
     */
    public function __construct(string $apiKeyIpify, string $apiKeyGoogle)
    {
        $this->apiKeyIpify = $apiKeyIpify;
        $this->apiKeyGoogle = $apiKeyGoogle;
    }



    function getCoordinatesFromAdress(string $address){



        // replace all the white space with "+" sign to match with google search pattern
        $address = str_replace(" ", "+", $address);

        $url = "https://maps.google.com/maps/api/geocode/json?sensor=false&key=$this->apiKeyGoogle&address=$address";
        $response = file_get_contents($url);

        // generate array object from the response from the web
        $json = json_decode($response,TRUE);
        // Latitude
        $latitude = ($json['results'][0]['geometry']['location']['lat']) ? $json['results'][0]['geometry']['location']['lat'] : '--';
        // Longitude
        $longitude = ($json['results'][0]['geometry']['location']['lng']) ? $json['results'][0]['geometry']['location']['lng'] : '--';

        return array($latitude ,$longitude);
    }


    function getCoordinatesFromIp(string $ip){


        $url = "https://geo.ipify.org/api/v1?apiKey=$this->apiKeyIpify&ipAddress=$ip";
        $response = file_get_contents($url);

        // generate array object from the response from the web
        $json = json_decode($response,TRUE);
        // Latitude
        $latitude = ($json['location']['lat']) ? $json['location']['lat'] : '--';
        // Longitude
        $longitude = ($json['location']['lng']) ? $json['location']['lng'] : '--';

        return array($latitude ,$longitude);
    }


    /**
     * Calculate Distance Between two points.
     */
    public  function distance(string $lat1, string $lng1,string $lat2,string $lng2, $unit = 'k') {
        $earth_radius = 6378137;   // Terre = sphère de 6378km de rayon
        $rlo1 = deg2rad($lng1);
        $rla1 = deg2rad($lat1);
        $rlo2 = deg2rad($lng2);
        $rla2 = deg2rad($lat2);
        $dlo = ($rlo2 - $rlo1) / 2;
        $dla = ($rla2 - $rla1) / 2;
        $a = (sin($dla) * sin($dla)) + cos($rla1) * cos($rla2) * (sin($dlo) * sin($dlo));
        $d = 2 * atan2(sqrt($a), sqrt(1 - $a));
        //
        $meter = ($earth_radius * $d);
        if ($unit == 'k') {
            return $meter / 1000;
        }
        return $meter;
    }

}
