<?php

namespace App\Translation;

use Symfony\Component\Serializer\NameConverter\CamelCaseToSnakeCaseNameConverter;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Translate class constants ; use the 'word' catalog.
 * 1. Build a translation string from the classname and constant name
 * 2. Send back the translations.
 */
class ClassConstantTranslator
{
    /**
     * @var TranslatorInterface
     */
    private $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    /**
     * Get the translations of the given class constants.
     * Constants can be filtered by their name and values.
     * Translation keys are build based on the class and constants names:
     * Example : for Class Question and constant CATEGORY_WL in word translation file add :
     *   question:
     *       category_wl:
     *           label: Hygiène de vie.
     *
     * @param string $className   fully-qualified class name
     * @param string $namePattern Filter the constants by name. You can use a regex which ill be used by a preg_match() function.
     * @param array  $values      filter the constants by values
     *
     * @return array Format: [cName1 => cTranslation1, cName2 => cTranslation2]
     *
     * @throws \ReflectionException
     */
    public function getTranslations(
        string $className,
        string $namePattern = '',
        array $values = [],
        string $translationDomain = 'action'
    ): array {
        $rc = new \ReflectionClass($className);
        $translations = [];
        $constants = $rc->getConstants();

        // filter the constant by name
        if ($namePattern) {
            $constants = \array_filter($constants, function ($name) use ($namePattern) {
                return false !== \preg_match("/$name/", $namePattern);
            }, ARRAY_FILTER_USE_KEY);
        }

        // filter the constant by value
        if ($values) {
            $constants = \array_intersect($constants, $values);
        }

        if (!$constants) {
            $translations[] = '';
        } else {
            foreach ($constants as $cName => $cValue) {
                $translation = $this->buildTranslationKey($cName, $rc->getShortName());

                // translate the key is a translation domain is provided
                if ($translationDomain) {
                    $translation = $this->translator->trans($translation, [], $translationDomain);
                }

                $translations[$cValue] = $translation;
            }
        }

        return $translations;
    }

    /**
     * Build the translation key of a class constant.
     * Example with the following fully-qualified class name App\Entity\Question (constant: CATEGORY_WL):
     * question.category_wl.label.
     */
    private function buildTranslationKey(string $constantName, string $className): string
    {
        return \join('.', [
            (new CamelCaseToSnakeCaseNameConverter([$className]))->normalize($className),
            \mb_strtolower($constantName),
            'label',
        ]);
    }
}
