<?php

namespace App\Provider;

use A2lix\TranslationFormBundle\Locale\LocaleProviderInterface;

class A2lixTranslationFormLocaleProvider implements LocaleProviderInterface
{
    /**
     * @var string
     */
    private $locale;

    /**
     * @var array
     */
    private $locales;

    public function __construct(string $locale, array $locales)
    {
        $this->locale = $locale;
        $this->locales = $locales;
    }

    public function getLocales(): array
    {
        return $this->locales;
    }

    public function getDefaultLocale(): string
    {
        return $this->locale;
    }

    public function getRequiredLocales(): array
    {
        // only the default front locale is required
        return [$this->locale];
    }
}
