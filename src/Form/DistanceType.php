<?php

namespace App\Form;


use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use App\Entity\Distance;
use Symfony\Component\Translation\TranslatorInterface;

class DistanceType extends AbstractType
{
    /**
     * @var TranslatorInterface
     */
    private $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('ipAdress', TextType::class, [
                                'attr' => [
                                    'placeholder' => 'adress.ip',
                                    'pattern' => '^(?:25[0-5]|2[0-4]\d|1\d\d|[1-9]\d|\d)(?:[.](?:25[0-5]|2[0-4]\d|1\d\d|[1-9]\d|\d)){3}$',
                                    ],

                    'label' => false,
                    ])
                ->add('postalAdress', TextType::class, [
                    'attr' => [
                        'placeholder' => 'adress.postal',
                        'pattern' => '\d{1,6}\s(?:[A-Za-z0-9#]+\s){1,7}(?:[A-Za-z0-9#]+,) \d{5}\s*(?:[A-Za-z]+\s){1,3}(?:[A-Za-z]+)\s*',
                    ],
                    'label' => false,
                    ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Distance::class,
            'translation_domain' => 'form',
        ]);
    }
}
