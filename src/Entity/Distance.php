<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Distance.
 *
 * @ORM\Entity(repositoryClass="App\Repository\DistanceRepository")
 */
class Distance
{

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;






    /**
     * @var string
     *
     * @ORM\Column(name="postalAdress", type="string")
     * @Assert\NotBlank(message="Distance.postalAdress.notBlank")
     */
    private $postalAdress;



    /**
     * @var string
     *
     * @ORM\Column(name="ipAdress", type="string", length=24)
     * @Assert\Length(min = 11,max = 20,  minMessage = "Distance.ipAdress")
     */
    private $ipAdress;



    public function __construct()
    {

    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dateStart.
     *
     * @param \DateTime $dateStart
     *
     * @return Distance
     */
    public function setDateStart($dateStart)
    {
        $this->dateStart = $dateStart;

        return $this;
    }

    /**
     * @return string
     */
    public function getPostalAdress()
    {
        return $this->postalAdress;
    }

    /**
     * @param string $postalAdress
     */
    public function setPostalAdress($postalAdress)
    {
        $this->postalAdress = $postalAdress;
    }

    /**
     * @return string
     */
    public function getIpAdress()
    {
        return $this->ipAdress;
    }

    /**
     * @param string $ipAdress
     */
    public function setIpAdress($ipAdress)
    {
        $this->ipAdress = $ipAdress;
    }








}
