<?php

namespace App\Twig;

use App\Translation\ClassConstantTranslator;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class ConstantTranslationExtension extends AbstractExtension
{
    /**
     * @var ClassConstantTranslator
     */
    private $classConstantTranslator;

    public function __construct(ClassConstantTranslator $classConstantTranslator)
    {
        $this->classConstantTranslator = $classConstantTranslator;
    }

    public function getFilters(): array
    {
        return [
            new TwigFilter('trans_const', [$this, 'getConstTranslation']),
        ];
    }

    public function getConstTranslation($value, $className, $namePattern = '', $translationDomain = 'word')
    {
        $isSingleValue = !\is_array($value);

        if ($isSingleValue) {
            $value = [$value];
        }

        $translations = $this->classConstantTranslator->getTranslations($className, $namePattern, $value, $translationDomain);

        return $isSingleValue ? \array_shift($translations) : $translations;
    }
}
