<?php

namespace App\Twig;

use Symfony\Component\Translation\TranslatorInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

/**
 * This extension returns an array of locales (as keys) and their translations (as values).
 * Translations are taken from the 'word' catalog.
 */
class LocalesTranslationsExtension extends AbstractExtension
{
    /**
     * @var string[]
     */
    private $locales;

    /**
     * @var TranslatorInterface
     */
    private $translator;

    public function __construct(TranslatorInterface $translator, array $locales)
    {
        $this->locales = $locales;
        $this->translator = $translator;
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('locales_translations', [$this, 'getLocalesTranslations']),
        ];
    }

    public function getLocalesTranslations(): array
    {
        $translations = \array_map(function (string $locale) {
            return $this->translator->trans(\sprintf('language.%s', $locale), [], 'word');
        }, $this->locales);

        return \array_combine($this->locales, $translations);
    }
}
