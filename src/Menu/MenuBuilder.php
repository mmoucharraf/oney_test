<?php

namespace App\Menu;

use App\Entity\InstitutionalContent\AbstractInstitutionalContent;
use App\Entity\InstitutionalContent\AbstractInstitutionalContentTranslation;
use App\Repository\InstitutionalContent\AbstractInstitutionalContentRepository;
use Knp\Menu\FactoryInterface;
use Knp\Menu\ItemInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Translation\TranslatorInterface;

class MenuBuilder
{
    /**
     * @var FactoryInterface
     */
    private $factory;



    /**
     * @var array
     */
    private $locales;

    /**
     * @var RequestStack
     */
    private $requestStack;

    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * @var TranslatorInterface
     */
    private $translator;

    public function __construct( FactoryInterface $factory, RequestStack $requestStack, RouterInterface $router, TranslatorInterface $translator, array $locales)
    {
        $this->factory = $factory;
        $this->locales = $locales;
        $this->requestStack = $requestStack;
        $this->router = $router;
        $this->translator = $translator;
    }

    /**
     * List every locale, and set the translated routes as link
     * Special behavior for institutional content single pages, as the 'slug' path token must be translated.
     */
    public function createLocalesMenu(): ItemInterface
    {
        $menu = $this->factory->createItem('root');
        $request = $this->requestStack->getMasterRequest();

        $currentRoute = $request->attributes->get('_route');
        $currentRouteParams = $request->attributes->get('_route_params');

        // special behavior for every route related to a single institutional content


        foreach ($this->locales as $locale) {


            $currentRouteParams['_locale'] = $locale;

            $menu->addChild($locale, [
                'uri' => $this->router->generate($currentRoute, $currentRouteParams),
                'label' => $this->translator->trans(\sprintf('language.%s', $locale), [], 'action'),
                'linkAttributes' => [
                    'class' => 'dropdown-item'.($locale === $request->getLocale() ? ' active' : ''),
                ],
            ]);
        }

        return $menu;
    }
}
