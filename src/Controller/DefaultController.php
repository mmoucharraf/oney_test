<?php

/**
 * Created by PhpStorm.
 * User: mounmouc
 * Date: 13/08/19
 * Time: 20:15
 */
namespace App\Controller;


use App\Entity\Distance;
use App\Entity\Shareddistance;
use App\Form\SharedistanceType;
use App\Form\DistanceType;
use App\Mail\ShareddistanceMailer;
use App\Repository\ShareddistanceRepository;
use App\Service\DistanceService;
use Doctrine\ORM\EntityManagerInterface;
use FOS\UserBundle\Model\UserManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Translation\TranslatorInterface;


class DefaultController extends Controller
{

    public function index(Request $request, RouterInterface $router, DistanceService $distanceService)
    {


        $distance = new Distance();

        $form = $this->createForm(DistanceType::class, $distance);
        $form->handleRequest($request);
        $dist=0;
        if ($form->isSubmitted() && $form->isValid()) {

            $corordiantaesIp=$distanceService->getCoordinatesFromIp($form->get('ipAdress')->getData());
            $corordiantaesAdresse=$distanceService->getCoordinatesFromAdress($form->get('postalAdress')->getData());

            $dist=$distanceService->distance($corordiantaesIp[0],$corordiantaesIp[1],$corordiantaesAdresse[0],$corordiantaesAdresse[1],'k');
            $this->addFlash('success', 'Voici la distance:');
        }

        return $this->render('layout-default.html.twig', [
            'form' => $form->createView(),'distance' =>$dist
        ]);


    }


}